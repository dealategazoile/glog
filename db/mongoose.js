const mongoose = require("mongoose");
require('dotenv').config()

const db_host = process.env.DB_HOST
const db_user = process.env.DB_USER
const db_pass = process.env.DB_PASS
const db_port = process.env.DB_PORT
const db_connect = "mongodb://"+db_user+":"+db_pass+"@"+db_host+":"+db_port+"/DB_GDL"
mongoose
  .connect(db_connect, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("connected to mongoDB"))
  .catch((err) => console.error("not conncted ...", err));

//***************************************************************************************************************************** */
/*
    const demandeurs_chaiba_schema = new mongoose.Schema({
        num: Number,
        date_doss: Date,
        d: String,
        c: String,
        Nom: String,
        prenom: String,
        sexe: String,
        ddn: String,
        ldn: String,
        sit_fam: String,
        ppere: String,
        nmere: String,
        pmere: String,
        cnom: String,
        cprenom: String,
        cddn: String,
        cldn: String,
        cppere: String,
        cnmere: String,
        cpmere: String
    })

    const demandeurs_chaiba = mongoose.model('Demandeurs_chaiba' , demandeurs_chaiba_schema, 'demandeurs_chaiba')

    async function creatDemandeursChaiba() {

    console.log("not implemented yet !!!!")
    }


    async function getDemandeurs_chaiba() {
    const demandeurs  = await demandeurs_chaiba
        .find()
        .skip((2 -1) * 5)
        .limit(5)


        console.log(demandeurs)

    }

    module.exports.get_demandeurs_chaiba = async function getDemandeurs_chaiba_by_name_and_surname (name_or_surname) {
        const demandeurs  = await demandeurs_chaiba
            .find().or([
              {Nom : {$regex: name_or_surname , $options: 'i' }} ,
              {prenom : {$regex: name_or_surname, $options: 'i' }},
              {cnom : {$regex: name_or_surname, $options: 'i' } } ,
              {cprenom : {$regex : name_or_surname, $options: 'i' }} ])


            return demandeurs
        }

    module.exports.get_demandeurs_chaiba_by_their_ddn = async function getDemandeurs_chaiba_by_their_ddn (ddn_entred) {
        console.log("fnct " + ddn_entred)
        const demandeurs  = await demandeurs_chaiba.find({ddn : '06/04/1960'})

                return demandeurs
            }

    async function updateCourse(id) {

        console.log("not implemented yet")
    }
*/
    //getDemandeurs_chaiba_by_name_and_surname("MEGLESLI")
    //vlcgetDemandeurs_chaiba_by_their_ddn2("06/04/1960")
    //************************************************************************************************************************************************************ */
    //creatCourse()
/*
    const demandeurs_kolea_schema = new mongoose.Schema({
        num: Number,
        date_doss: Date,
        d: String,
        c: String,
        Nom: String,
        prenom: String,
        sexe: String,
        ddn: String,
        ldn: String,
        sit_fam: String,
        ppere: String,
        nmere: String,
        pmere: String,
        cnom: String,
        cprenom: String,
        cddn: String,
        cldn: String,
        cppere: String,
        cnmere: String,
        cpmere: String
    })

    const demandeurs_kolea = mongoose.model('Demandeurs_kolea' , demandeurs_kolea_schema, 'demandeurs_kolea')

    async function creatDemandeursKolea() {

    console.log("not implemented yet !!!!")
    }


    async function getDemandeurs_kolea() {
    const demandeurs  = await demandeurs_kolea
        .find()
        .skip((2 -1) * 5)
        .limit(5)


        console.log(demandeurs)

    }

    module.exports.get_demandeurs_kolea = async function getDemandeurs_kolea_by_name_and_surname (name_or_surname) {
        const demandeurs  = await demandeurs_kolea
            .find().or([
              {Nom : {$regex: name_or_surname , $options: 'i' }} ,
              {prenom : {$regex: name_or_surname, $options: 'i' }},
              {cnom : {$regex: name_or_surname, $options: 'i' } } ,
              {cprenom : {$regex : name_or_surname, $options: 'i' }} ])


            return demandeurs
        }

    module.exports.get_demandeurs_kolea_by_their_ddn = async function getDemandeurs_kolea_by_their_ddn (ddn_entred) {

            const demandeurs  = await demandeurs_kolea.find().or([
              {ddn :   {$regex: ddn_entred , $options: 'i' }},
              {cddn : {$regex: ddn_entred , $options: 'i' } },
            ])
                    return demandeurs
                }
    async function updateDemandeurKolea(id) {

        console.log("not implemented yet")
    }
    */
    //getDemandeurs_kolea_by_name_and_surname("BOUDALI")

    //************************************************************************************************************************************* */
/*
    const demandeurs_attatba_schema = new mongoose.Schema({
        num: Number,
        date_doss: Date,
        d: String,
        c: String,
        Nom: String,
        prenom: String,
        sexe: String,
        ddn: String,
        ldn: String,
        sit_fam: String,
        ppere: String,
        nmere: String,
        pmere: String,
        cnom: String,
        cprenom: String,
        cddn: String,
        cldn: String,
        cppere: String,
        cnmere: String,
        cpmere: String
    })

    const demandeurs_attatba = mongoose.model('Demandeurs_attatba' , demandeurs_attatba_schema, 'demandeurs_attatba')

    async function creatDemandeursAttatba() {

    console.log("not implemented yet !!!!")
    }


    async function getDemandeurs_attatba() {
    const demandeurs  = await demandeurs_attatba
        .find()
        .skip((2 -1) * 5)
        .limit(5)


        console.log(demandeurs)

    }

    module.exports.get_demandeurs_attatba = async function getDemandeurs_attatba_by_name_and_surname (name_or_surname) {
        const demandeurs  = await demandeurs_attatba
            .find().or([
              {Nom : {$regex: name_or_surname , $options: 'i' }} ,
              {prenom : {$regex: name_or_surname, $options: 'i' }},
              {cnom : {$regex: name_or_surname, $options: 'i' } } ,
              {cprenom : {$regex : name_or_surname, $options: 'i' }} ])

            return demandeurs
        }

    module.exports.get_demandeurs_attatba_by_their_ddn = async function getDemandeurs_attatba_by_their_ddn (ddn_entred) {
                const demandeurs  = await demandeurs_attatba.find().or([
                  {ddn :   {$regex: ddn_entred , $options: 'i' }},
                  {cddn : {$regex: ddn_entred , $options: 'i' } },
                ])
                        return demandeurs
    }

    async function updateDemandeurAttatba(id) {

        console.log("not implemented yet")
    }
*/
    //getDemandeurs_attatba_by_name_and_surname('KOUIDER')
