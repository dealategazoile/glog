const express = require('express')
const app = express()
const db = require('./db/mongoose')
const demandeurs_chaiba = require('./routes/demandeurs_chaiba_router')
const demandeurs_kolea = require('./routes/demandeurs_kolea_router')
const demandeurs_attatba = require('./routes/demandeurs_attatba_router')

app.get('/', function (req, res) {
  var atta = db.get_demandeurs_attatba("KOUIDER")
  console.log(atta)
  res.send(atta)

})

app.use('/api/chaiba', demandeurs_chaiba)
app.use('/api/kolea', demandeurs_kolea)
app.use('/api/attatba', demandeurs_attatba)

app.listen(3000, console.log("linstening on ..."))
