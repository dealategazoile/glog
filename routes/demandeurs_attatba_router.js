const router = require('express').Router()
const demandeurs_attatba = require('../model/demandeurs_attatba_model')


router.get('/', function (req, res) {
  const nom = req.query.nom
  var resultat
  if(nom){
    demandeurs_attatba.get_demandeurs_attatba(nom).then((value) => {
      resultat = value;
      return res.send(resultat)

})
.catch((error) => {
    console.log("erreur" +error)
  });
  }


})

router.get('/ddn' , function (req , res){
  const ddn = req.query.ddn_entred
  var resultat
  if(ddn){
    demandeurs_attatba.get_demandeurs_attatba_by_their_ddn(ddn).then((value) =>{
      resultat = value
      return res.send(resultat)
    })
    .catch((erreur)=>console.log("erreur "+ erreur))}
})



module.exports = router
