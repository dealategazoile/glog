const router = require("express").Router();
const demandeur_chaiba = require("../model/demandeurs_chaiba_model");

router.get("/", function (req, res) {
  const nom = req.query.nom;
  const ddn = req.query.ddn;
  var resultat;
  if (nom) {
    demandeur_chaiba
      .get_demandeurs_chaiba(nom)
      .then((value) => {
        resultat = value;
        return res.send(resultat);
      })
      .catch((error) => {
        console.log("erreur" + error);
      });
  }



})

router.get('/ddn' , function (req , res){
  const ddn = req.query.ddn_entred
  var resultat
  if(ddn){
    demandeurs_chaiba.get_demandeurs_chaiba_by_their_ddn(ddn).then((value) =>{
      resultat = value
      return res.send(resultat)
    })
    .catch((erreur)=>console.log("erreur "+ erreur))}
})


module.exports = router;
