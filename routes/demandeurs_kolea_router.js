const router = require('express').Router()
const demandeurs_kolea = require('../model/demandeurs_kolea_model')


router.get('/', function (req, res) {
  const nom = req.query.nom
  var resultat
  if(nom){
    demandeurs_kolea.get_demandeurs_kolea(nom).then((value) => {
      resultat = value;
      return res.send(resultat)

})
.catch((error) => {
    console.log("erreur" +error)
  });
  }


})

router.get('/ddn' , function (req , res){
  const ddn = req.query.ddn_entred
  var resultat
  if(ddn){
    demandeurs_kolea.get_demandeurs_kolea_by_their_ddn(ddn).then((value) =>{
      resultat = value
      return res.send(resultat)
    })
    .catch((erreur)=>console.log("erreur "+ erreur))}
})


module.exports = router
